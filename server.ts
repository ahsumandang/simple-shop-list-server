import { router } from './router';
const express = require('express');
const mongoose = require("mongoose")

const app = express();
const port = 8000;
app.listen(port, () => {
	console.log(`Listening to port ${port}!`)
});

// Set up application routes
app.use(router);

// Connect to online database
const onlineDatabaseURL = 'mongodb+srv://guest:guestpw@cluster0.px146.mongodb.net/shop';
mongoose.connect(onlineDatabaseURL, {
	useUnifiedTopology: true,
	useNewUrlParser: true,
	authSource: "admin",
	readPreference: "primary",
	ssl: true
}).then(() => {
	console.log('DB connected!')
}).catch((err: any) => {
	console.log(`DB Connection Error: ${err.message}`)
})