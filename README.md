# ShopApp Server

The server-side application of an app where you can view the list of products and add them to your cart.

## How to Set Up

### Prerequisite
Make sure the following technologies are set up in your local. You can use higher version but this is my local version.
- Node (v14.15.4)
- NPM (6.14.10)

### Execution Steps

1. In the command line of your choice, go to `/api` directory and type `npm install` (requires internet). Ensure that there are no error messages.
2. Type in `npm start`. Make sure that this is running properly as indicated by the message "Listening to port 8000" and "DB connected". This step also requires internet because app is using cloud database.
2. Go to [localhost:8000](http://localhost:8000) for further checking if the server is working. Use existing endpoints to check (e.g.  [endpoint for retrieving all products](http://localhost:8000/api/products/all))