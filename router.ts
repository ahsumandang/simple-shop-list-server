import { Router } from "express";
import * as ProductController from "./controllers/products";
const router = Router();

// Get products
router.get('/api/products/all', ProductController.getAllProducts);
router.get('/api/products/:productID', ProductController.getProduct);

export { router }