import { Request, Response } from 'express';
import { Product } from '../models/product';

/**
 * Gets all products from the database.
 */
export function getAllProducts(req: Request, res: Response) {
	Product.find().then(products => res.send(products));
};

export function getProduct(req: Request, res: Response) {
	const productID = req.params.productID;
	res.send({ hello: 'single prodct' });
};