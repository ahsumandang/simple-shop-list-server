import mongoose = require("mongoose");

export interface IProduct extends mongoose.Document {
	name: string;
	price: number;
	description: string;
	photo: string;
}

export const ProductSchema = new mongoose.Schema({
	name: {
		type: String,
		required: true
	},
	price: {
		type: Number,
		required: true
	},
	description: {
		type: String,
		required: true
	},
	photo: {
		type: String,
		required: true
	}
});

const Product = mongoose.model<IProduct>("Product", ProductSchema);
export { Product }